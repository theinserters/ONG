
package controller;



import static controller.NewFXMain.ong;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import model.ConexionDAO;
import model.Ong;


public class ConexionXML implements ConexionDAO {

    @Override
    public void conectar() {
        //
    }

    @Override
    public void desconectar() {
        //
    }
    
    @Override
    public void save(Ong ong){
         
        try {
            JAXBContext context = JAXBContext.newInstance(Ong.class);
            Marshaller marshaller = context.createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(ong, new File("ong.xml"));
            System.out.println("�Guardado exitosamente!");
            
        } catch (JAXBException ex) {
            Logger.getLogger(ConexionXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Override
    public void load(){
        
        try {
            JAXBContext context = JAXBContext.newInstance(Ong.class); 
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Ong p;
            p = (Ong) unmarshaller.unmarshal(new File("ong.xml"));
            System.out.println("�Cargado exitosamente!");
            
            ong.setPersonas(p.getPersonas());
            ong.setProyectos(p.getProyectos());
            p = null;
            
        } catch (JAXBException ex) {
            //Logger.getLogger(ConexionXML.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("�No existe el fichero!");
        }
    }


}
