
package controller;

import model.CRUD;
import model.Colaborador;
import model.Persona;
import model.Socio;
import model.Trabajador;
import model.Voluntario;


public class FactoryPersona {
    
    public CRUD<Persona> getPersona(Object t){
        if(t instanceof Voluntario) return new VoluntarioDAOImpl();
        else if(t instanceof Trabajador) return new TrabajadorDAOImpl();
        else if(t instanceof Socio) return new SocioDAOImpl();
        else if(t instanceof Colaborador) return new ColaboradorDAOImpl();
        return null;
    }
}
