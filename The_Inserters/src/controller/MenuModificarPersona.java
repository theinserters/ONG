/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Colaborador;
import model.Persona;
import model.QueryHibernate;
import model.SavePersona;
import model.Socio;
import model.Trabajador;
import model.Voluntario;
import org.hibernate.Session;

/**
 *
 * @author Darkty
 */
public class MenuModificarPersona implements Initializable{
    
    public static Persona personaM;

    @FXML private ComboBox tipo;
    @FXML private TextField id_persona;
    @FXML private TextField dni;
    @FXML private TextField apellidos;
    @FXML private TextField nombre;
    @FXML private Label etiqueta_repetido;
    @FXML private CheckBox pagado;
    //@FXML private Button boton_guardar;
    
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tipo.getItems().add("Colaborador");
        tipo.getItems().add("Socio");
        tipo.getItems().add("Trabajador");
        tipo.getItems().add("Voluntario");

        tipo.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                tipo.setValue(newValue);
            }
        }); 
    }
    
    public void setPersona(Persona persona){
        this.personaM = persona;
        this.id_persona.setText(persona.getId_persona());
        this.nombre.setText(persona.getNombre());
        this.apellidos.setText(persona.getApellidos());
        this.dni.setText(persona.getDni());
        this.pagado.setText(persona.getPagado());
        this.tipo.setValue(persona.getTipo());;
        if (persona.getTipo().equalsIgnoreCase("Socio")){
            this.pagado.setVisible(true);
        }else this.pagado.setVisible(false);
    }
    
    
    public void ComboUpdated(){ 
        Long count = QueryHibernate.comboUpdate(tipo);
        
        if (tipo.getValue()== "Voluntario"){
           this.id_persona.setText("V" + (count + 1));
           pagado.setVisible(false);
           //tipo.setValue("Voluntario");
           Voluntario p = new Voluntario();
        }else if (tipo.getValue()== "Colaborador"){
           this.id_persona.setText("C" + (count + 1));
           pagado.setVisible(false);
           //tipo.setValue("Colaborador");
           Colaborador p = new Colaborador();
        }else if (tipo.getValue()== "Socio"){
           this.id_persona.setText("S" + (count + 1));
           //tipo.setValue("Socio");
           pagado.setVisible(true);
           pagado.setText("PAGADO");
           Socio p = new Socio();
        }else if (tipo.getValue()== "Trabajador"){
           this.id_persona.setText("T" + (count + 1));
           //tipo.setValue("Trabajador");
           pagado.setVisible(false);
           Trabajador p = new Trabajador();
        }
    }
    
    public void guardarPersonaModificar() throws IOException{
        SavePersona.guardarPersona(personaM, id_persona, tipo, dni, apellidos, nombre, pagado);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Actualizar Persona");
        alert.setHeaderText(null);
        alert.setContentText("Datos actualizados correctamente.");
        alert.showAndWait();
    }
    
    
    public void limpiarFormularioModificar(){
        id_persona.setText(personaM.getId_persona());
        tipo.setValue(personaM.getTipo());
        //tipo.getItems().clear();
        dni.setText(personaM.getDni());
        apellidos.setText(personaM.getApellidos());
        nombre.setText(personaM.getNombre());
        
        //pagado.setSelected(false);
        pagado.setVisible(true);
        etiqueta_repetido.setVisible(false);
        //boton_guardar.setDisable(true);
        tipo.setFocusTraversable(true);
    }


}
