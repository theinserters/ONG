
package controller;

import model.SocioDAO;
import model.Persona;
import model.Socio;


public class SocioDAOImpl implements SocioDAO {
    
    @Override
    public void pagarCuota(){
        //Pagar la cuota
    }

    @Override
    public void imprimirDatos(Persona t) {
        //System.out.println("Socio: "+t.toString());
        Socio o = (Socio)t;
        System.out.println("Socio: "+o.getNumSocio()+" - "+o.getDni());
    }

    @Override
    public String recuperarCodigo(Persona t){
        Socio v = (Socio)t;
        return v.getNumSocio();
    }
    
}
