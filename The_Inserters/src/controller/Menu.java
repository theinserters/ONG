package controller;

import static controller.NewFXMain.stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
 


public class Menu implements Initializable{
    
    @Override 
    public void initialize(URL location, ResourceBundle resources) {}
    
    public void handleClick(MouseEvent event){}
    
    public void salirAplicacion(MouseEvent event) throws IOException{
        Platform.exit();
    }    
    
    public void abrirConsulta(MouseEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("../Vista/Consultas.fxml"));
        Scene scene = new Scene(root);
        
        stage.setTitle("The Inserters");
        stage.setScene(scene);
        stage.show();
    }
    
    public void abrirAltaPersona(MouseEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("../Vista/AltasPersona.fxml"));
        Scene scene = new Scene(root);
        
        stage.setTitle("The Inserters");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void abrirAltaProyecto(MouseEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("../Vista/AltasProyectos.fxml"));
        Scene scene = new Scene(root);
        
        stage.setTitle("The Inserters");
        stage.setScene(scene);
        stage.showAndWait();
    }




    
    
}

