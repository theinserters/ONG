
package controller;

import java.io.IOException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import model.Ong;
import model.Proyecto;
import model.QueryHibernate;

@SuppressWarnings("unchecked")
public class NewFXMain extends Application {
    
    //private final TableView<Proyecto> table = new TableView<>();
    //private ObservableList<Proyecto> data = FXCollections.observableArrayList();
    
    public static Stage stage = new Stage();
    
    public static Ong ong = new Ong("Entreculturas", 20000.0);
    
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource("../Vista/Consultas.fxml"));
        Scene scene = new Scene(root);
        
        stage.setTitle("The Inserters");
        stage.setScene(scene);
        stage.show();
        
    }

    
}
