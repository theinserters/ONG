/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import static controller.NewFXMain.ong;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Persona;
import model.Proyecto;
import model.Proyecto_Persona;

import model.QueryHibernate;
 


public class MenuConsultas implements Initializable{
    
    private final QueryHibernate query = new QueryHibernate();
    private ObservableList<Proyecto> dataProyecto = FXCollections.observableArrayList();
    private ObservableList<Persona> dataPersona = FXCollections.observableArrayList();
    private ObservableList<Proyecto_Persona> dataPerPro = FXCollections.observableArrayList();

    private Persona persona;
    private Proyecto proyecto;
    private Proyecto_Persona proyectopersona;
    
    @FXML private TableView<Proyecto> tablaProyectos; 
    @FXML private TableColumn<Proyecto, Integer> codigo_proyecto; 
    @FXML private TableColumn<Proyecto, String> pais; 
    @FXML private TableColumn<Proyecto, String> localizacion;
    @FXML private TableColumn<Proyecto, Proyecto.LineaAccion> linea_de_accion;
    @FXML private TableColumn<Proyecto, Proyecto.SubLineaAccion> sublinea_de_accion;
    @FXML private TableColumn<Proyecto, Date> fecha_de_inicio;
    @FXML private TableColumn<Proyecto, Date> fecha_de_finalizacion;
    @FXML private TableColumn<Proyecto, Float> financiacion_aportada;
    
    @FXML private TableView<Persona> tablaPersonas; 
    @FXML private TableColumn<Persona, String> id_persona;
    @FXML private TableColumn<Persona, String> dni; 
    @FXML private TableColumn<Persona, String> apellidos;
    @FXML private TableColumn<Persona, String> nombre;
    @FXML private TableColumn<Persona, String> tipo;
    @FXML private TableColumn<Persona, String> pagado;
    
    @FXML private TableView<Proyecto_Persona> tablaPerPro; 
    @FXML private TableColumn<Proyecto_Persona, String> personas_dni;
    @FXML private TableColumn<Proyecto_Persona, Integer> proyecto_codigo_proyecto; 
    
    @FXML private Button refresh;
    
    //@SuppressWarnings("unchecked")
    @Override 
    public void initialize(URL location, ResourceBundle resources){ 
        
        //CODIGO PARA TABLA PROYECTOS
        codigo_proyecto.setCellValueFactory(new PropertyValueFactory<>("codigo_proyecto")); 
        pais.setCellValueFactory(new PropertyValueFactory<>("pais")); 
        localizacion.setCellValueFactory(new PropertyValueFactory<>("localizacion"));
        linea_de_accion.setCellValueFactory(new PropertyValueFactory<>("linea_de_accion"));
        sublinea_de_accion.setCellValueFactory(new PropertyValueFactory<>("sublinea_de_accion"));
        fecha_de_inicio.setCellValueFactory(new PropertyValueFactory<>("fecha_inicio"));
        fecha_de_finalizacion.setCellValueFactory(new PropertyValueFactory<>("fecha_finalizacion"));
        financiacion_aportada.setCellValueFactory(new PropertyValueFactory<>("financiacion_aportada"));
        //tablaProyectos.getColumns().addAll(codigo_proyecto, pais, localizacion, linea_de_accion, sublinea_de_accion, fecha_de_inicio, fecha_de_finalizacion);

        //CODIGO PARA TABLA PERSONAS
        id_persona.setCellValueFactory(new PropertyValueFactory<>("id_persona"));
        dni.setCellValueFactory(new PropertyValueFactory<>("dni")); 
        apellidos.setCellValueFactory(new PropertyValueFactory<>("apellidos"));
        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        pagado.setCellValueFactory(new PropertyValueFactory<>("pagado"));
        //tablaPersonas.getColumns().addAll(id_persona, dni, apellidos, nombre, tipo, pagado);
        pagado.setVisible(true);
        
        //CODIGO PARA PERSONAS ASIGNADAS A UN PROYECTO
        personas_dni.setCellValueFactory(new PropertyValueFactory<>("personas_id"));
        proyecto_codigo_proyecto.setCellValueFactory(new PropertyValueFactory<>("proyecto_codigo_proyecto"));
        //tablaPerPro.getColumns().addAll(codigo_proyecto_p, dni_persona_p);
        
        //MENU CONTEXTUAL PARA PERSONAS
        final ContextMenu tableContextMenuPersona = new ContextMenu();
        final MenuItem addPersona = new MenuItem("Nueva");
        final MenuItem modificarPersona = new MenuItem("Modificar");
        final MenuItem eliminarPersona = new MenuItem("Eliminar");
        final MenuItem asignarPersona = new MenuItem("Asignar a proyecto");
        addPersona.setOnAction(e -> {
            try {
                altaPersona();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        modificarPersona.setOnAction(e -> {
            try {
                persona = tablaPersonas.getSelectionModel().getSelectedItem();
                modificarPersona();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        eliminarPersona.setOnAction(e -> {
            persona = tablaPersonas.getSelectionModel().getSelectedItem();
            eliminarPersona();
        });
        asignarPersona.setOnAction(e -> {
            try {
                persona = tablaPersonas.getSelectionModel().getSelectedItem();
                proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
                altaPersonaProyecto();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        tableContextMenuPersona.getItems().addAll(addPersona, modificarPersona, eliminarPersona, asignarPersona);
        tablaPersonas.setContextMenu(tableContextMenuPersona);

        
        //MENU CONTEXTUAL PARA ASIGNAR PERSONAS A PROYECTOS
        final ContextMenu tableContextMenuPersonaProyecto = new ContextMenu();
        final MenuItem asignarPersonaProyecto = new MenuItem("Asignar");
        final MenuItem eliminarPersonaProyecto = new MenuItem("Eliminar");
        asignarPersonaProyecto.setOnAction(e -> {
            try {
                persona = tablaPersonas.getSelectionModel().getSelectedItem();
                proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
                altaPersonaProyecto();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        eliminarPersonaProyecto.setOnAction(e -> {
            proyectopersona = tablaPerPro.getSelectionModel().getSelectedItem();
            eliminarPersonaProyecto();
        });
        tableContextMenuPersonaProyecto.getItems().addAll(asignarPersonaProyecto, eliminarPersonaProyecto);
        tablaPerPro.setContextMenu(tableContextMenuPersonaProyecto);
        

        //MENU CONTEXTUAL PARA PROYECTOS
        final ContextMenu tableContextMenuProyecto = new ContextMenu();
        final MenuItem addProyecto = new MenuItem("Nuevo");
        final MenuItem modificarProyecto = new MenuItem("Modificar");
        final MenuItem eliminarProyecto = new MenuItem("Eliminar");
        addProyecto.setOnAction(e -> {
            try {
                altaProyecto();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        modificarProyecto.setOnAction(e -> {
            try {
                proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
                modificarProyecto();
            } catch (IOException ex) {
                Logger.getLogger(MenuConsultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        eliminarProyecto.setOnAction(e -> {
            proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
            eliminarProyecto();
        });
        tableContextMenuProyecto.getItems().addAll(addProyecto, modificarProyecto, eliminarProyecto);
        tablaProyectos.setContextMenu(tableContextMenuProyecto);
        
        
        //REFRESCA LAS TABLAS
        refresh.setOnAction((ActionEvent e) -> {
            loadAll();
        });

        
        //INTRODUCE EL CONTENIDO EN LAS TABLAS
        List<Proyecto_Persona> listaPerPro = query.getPerPro();
        dataPerPro = FXCollections.observableList(listaPerPro);  //<- activar oscar
        tablaPerPro.setItems(dataPerPro);

        List<Persona> listaPersona = query.getPersonas();
        dataPersona = FXCollections.observableList(listaPersona);
        tablaPersonas.setItems(dataPersona);

        List listaProyecto = query.getProyectos();
        dataProyecto = FXCollections.observableList(listaProyecto);
        tablaProyectos.setItems(dataProyecto);
        
    }

    public void saveAll(ActionEvent event){
        query.saveAll(ong);
    }
    
    public void loadAll(){
        //query.loadAll(ong);
        dataPersona.clear();
        dataPersona.removeAll(dataPersona);
        List<Persona> listaPersona = query.getPersonas();
        dataPersona = FXCollections.observableList(listaPersona);
        tablaPersonas.setItems(dataPersona);
        tablaPersonas.refresh();

        dataProyecto.clear();
        dataProyecto.removeAll(dataProyecto);
        List<Proyecto> listaProyecto = query.getProyectos();
        dataProyecto = FXCollections.observableList(listaProyecto);
        tablaProyectos.setItems(dataProyecto);
        tablaProyectos.refresh();
        
        dataPerPro.clear();
        dataPerPro.removeAll(dataPerPro);
        List listaPerPro = query.getPerPro();
        dataPerPro = FXCollections.observableList(listaPerPro);
        tablaPerPro.setItems(dataPerPro);
        tablaPerPro.refresh();
    }

    
    public void altaPersonaProyecto() throws IOException{
        persona = tablaPersonas.getSelectionModel().getSelectedItem();
        proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
        if (proyecto==null || persona==null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Asignación");
            alert.setHeaderText(null);
            alert.setContentText("Debe seleccionar una persona y un proyecto.");
            alert.showAndWait();
        }
        else{
            FXMLLoader loader = new FXMLLoader();
            Stage stage = new Stage();
            VBox vbox = (VBox)loader.load(getClass().getResource("../Vista/AltasPersonaProyecto.fxml").openStream());
            MenuAltasPersonaProyecto altaPersonaProyecto = loader.getController(); //new MenuModificarProyecto(); 
            loader.setController(altaPersonaProyecto);
            altaPersonaProyecto.setPersonaProyecto(persona, proyecto);
            Scene scene = new Scene(vbox);
            stage.setScene(scene);
            stage.show();
        }
    }
    
    
    public void eliminarPersonaProyecto(){
        proyectopersona = tablaPerPro.getSelectionModel().getSelectedItem();
        if (proyectopersona==null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Eliminar asignación");
            alert.setHeaderText(null);
            alert.setContentText("Debe seleccionar una persona asignada a un proyecto.");
            alert.showAndWait();
        }
        else{
            QueryHibernate.deletePersonaProyecto(proyectopersona);
        }
    }
    
    
    public void altaPersona() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        Stage stage = new Stage();
        VBox vbox = (VBox)loader.load(getClass().getResource("../Vista/AltasPersona.fxml").openStream());
        Scene scene = new Scene(vbox);
        stage.setScene(scene);
        stage.show();
    }
    
    
    public void altaProyecto() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        Stage stage = new Stage();
        VBox vbox = (VBox)loader.load(getClass().getResource("../Vista/AltasProyectos.fxml").openStream());
        Scene scene = new Scene(vbox);
        stage.setScene(scene);
        stage.show();
    }
    
    public void modificarPersonaMenu() throws IOException{
        persona = tablaPersonas.getSelectionModel().getSelectedItem();
        modificarPersona();
        loadAll();
    }
    
    public void eliminarPersonaMenu() throws IOException{
        persona = tablaPersonas.getSelectionModel().getSelectedItem();
        eliminarPersona();
        loadAll();
    }
    
    public void modificarPersona() throws IOException{
        persona = tablaPersonas.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader();
        Stage stage = new Stage();
        VBox second = (VBox)loader.load(getClass().getResource("../Vista/ModificarPersona.fxml").openStream());
        MenuModificarPersona menuModificarPersona = loader.getController(); //new MenuModificarPersona();
        loader.setController(menuModificarPersona);
        menuModificarPersona.setPersona(persona);
        Scene scene = new Scene(second);
        stage.setScene(scene);
        stage.show();
        
    }

    public void modificarProyectoMenu() throws IOException{
        proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
        modificarProyecto();
        loadAll();
    }
    
    
    public void eliminarProyectoMenu() throws IOException{
        proyecto = tablaProyectos.getSelectionModel().getSelectedItem();
        eliminarProyecto();
        loadAll();
    }
    
    public void modificarProyecto() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        Stage stage = new Stage();
        VBox third = loader.load(getClass().getResource("../Vista/ModificarProyecto.fxml").openStream());
        MenuModificarProyecto menuModificarProyecto = loader.getController(); //new MenuModificarProyecto(); 
        loader.setController(menuModificarProyecto);
        menuModificarProyecto.setProyecto(proyecto);
        Scene scene3 = new Scene(third);
        stage.setScene(scene3);
        stage.show();
    }
    
    public void eliminarPersona(){
        QueryHibernate.deletePersona(persona);
        loadAll();
    }
    
    public void eliminarProyecto(){
        QueryHibernate.deleteProyecto(proyecto);
        loadAll();
    }
    
    public void cerrarAplicacion(){
        Platform.exit();
    }
    

    
}