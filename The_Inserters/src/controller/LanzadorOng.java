
package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import model.Ong;
import model.Persona;
import model.Proyecto;
import javax.xml.bind.*;

public class LanzadorOng {

    private static Ong ong;

    public static void main(String[] args) throws JAXBException, IOException {
        ong = new Ong("Entreculturas", 2000.0);
        menu();
    }
    
    public static void menu() throws IOException{
        
        Scanner entrada = new Scanner(System.in);
        ConexionFactory factory = new ConexionFactory();
        
            int n1;
            do{
                System.out.println(""
                        + "**************************************\n"
                        + "Menú:\n"
                        + "1. XML y BBDD...\n"
                        + "2. Personas...\n"
                        + "3. Proyectos...\n"
                        + "0. Salir\n"
                        + "**************************************");
                n1 = entrada.nextInt();
                switch(n1){
                    
                    case 1:
                        int n2;
                        do{
                            System.out.println(""
                                    + "**************************************\n"
                                    + "Submenú XML y BBDD:\n"
                                    + "1. Mostrar fichero en formato XML\n"
                                    + "2. Salvar en XML\n"
                                    + "3. Cargar desde XML\n"
                                    + "4. Salvar en BBDD\n"
                                    + "5. Cargar desde BBDD\n"
                                    + "0. Volver...\n"
                                    + "**************************************");
                            n2 = entrada.nextInt();
                            switch(n2){
                                case 1:
                                    showFile();
                                    break;
                                case 2:
                                    factory.getConexion("XML").save(ong);
                                    break;
                                case 3:
                                    
                                    if (new File("ong.xml").exists() ){
                                        factory.getConexion("XML").load();
                                        factory.getConexion("MySQL").save(ong);
                                    }
                                    break;
                                case 4:
                                    factory.getConexion("MySQL").save(ong);
                                    break;
                                case 5:
                                    factory.getConexion("MySQL").load();
                                    break;
                            }
                        }while(n2 != 0);
                        break;
                        
                    case 2:
                        int n3;
                        do{
                            System.out.println(""
                                    + "**************************************\n"
                                    + "Submenú personas:\n"
                                    + "1. Mostrar datos de Persona\n"
                                    + "2. Crear Persona\n"
                                    + "3. Eliminar Persona\n"
                                    + "0. Volver...\n"
                                    + "**************************************");
                            n3 = entrada.nextInt();
                            switch(n3){
                                case 1:
                                    if(ong.getPersonas().size()>0){
                                        for (Persona p: ong.getPersonas()){
                                            System.out.println(p.toString());
                                        }
                                    }else System.out.println("¡No existen personas que mostrar!");
                                    break;
                                case 2:
                                    ong.crearPersona(ong);
                                    break;
                                case 3:
                                    ong.eliminarPersona(ong);
                                    break;
                            }
                        }while(n3 != 0);
                        break;
                        
                    case 3:
                        int n4;
                        do{
                            System.out.println(""
                                    + "**************************************\n"
                                    + "Submenú proyectos:\n"
                                    + "1. Mostrar Proyectos\n"
                                    + "2. Crear Proyecto\n"
                                    + "3. Eliminar Proyecto\n"
                                    + "4. Asignar Persona a Proyecto\n"
                                    + "0. Volver...\n"
                                    + "**************************************");
                            n4 = entrada.nextInt();
                            switch(n4){
                                case 1:
                                    if(ong.getProyectos().size()>0){
                                        for (Proyecto p: ong.getProyectos()){
                                            System.out.println(p.toString());
                                        }
                                    }else System.out.println("¡No existen proyectos que mostrar!");
                                    break;
                                case 2:
                                    ong.crearProyecto(ong);
                                    break;
                                case 3:
                                    ong.eliminarProyecto(ong);
                                    break;
                                case 4:
                                    ong.asignarPersonaProyecto(ong);
                                    break;
                            }
                        }while(n4 != 0);
                        break;
                }
            }while(n1 != 0);
        
    }
    
    public static void showFile() throws IOException{
        try {
            String linea;
            FileReader f = new FileReader("ong.xml");
            BufferedReader buffer = new BufferedReader(f);
            while((linea = buffer.readLine())!= null) {
                System.out.println(linea);
            }     
            buffer.close();
            f.close();
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(LanzadorOng.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("¡No existe el fichero!\n");
        }
    }
    
    
    
    
}
