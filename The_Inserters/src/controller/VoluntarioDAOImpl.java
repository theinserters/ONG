
package controller;

import model.VoluntarioDAO;
import model.Persona;
import model.Voluntario;


public class VoluntarioDAOImpl implements VoluntarioDAO{
    
    @Override
    public void imprimirDatos(Persona t) {
        //System.out.println("Voluntario: "+t.toString());
        Voluntario o = (Voluntario)t;
        System.out.println("Voluntario: "+o.getNumVoluntario()+" - "+o.getDni());
    }
    
    @Override
    public String recuperarCodigo(Persona t){
        Voluntario v = (Voluntario)t;
        return v.getNumVoluntario();
    }
}
