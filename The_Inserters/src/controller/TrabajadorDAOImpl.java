
package controller;

import model.TrabajadorDAO;
import model.Persona;
import model.Trabajador;


public class TrabajadorDAOImpl implements TrabajadorDAO{
    
    @Override
    public void imprimirDatos(Persona t) {
        //System.out.println("Trabajador: "+t.toString());
        Trabajador o = (Trabajador)t;
        System.out.println("Trabajador: "+o.getNumTrabajador()+" - "+o.getDni());
    }
    
    @Override
    public String recuperarCodigo(Persona t){
        Trabajador v = (Trabajador)t;
        return v.getNumTrabajador();
    }
}
