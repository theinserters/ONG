
package controller;


import static controller.NewFXMain.ong;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Ong;
import model.ConexionDAO;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.text.ParseException;


public class ConexionMySQL implements ConexionDAO{
    
    private String host;
    private String port;
    private String user;
    private String pass;
    private String schema;
    private Connection conexion;
    private Utilidad util;

    public ConexionMySQL() {
        this.host = "localhost";
        this.port= "3306";
        this.user = "inserters";
        this.pass = "Inserters0";
        this.schema = "inserters";
    }

    @Override
    public void conectar() {
        //Conectar a mysql
        try {
            //PASO 1. Creamos la conexion
            conexion = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+schema, user, pass);
            util = new Utilidad(conexion);
            /*if (conexion.isClosed() == false){
                System.out.println("Conexion Establecida");
            }*/
        }catch (SQLException ex) {
            System.out.println("No se ha podido conectar a la BBDD");
            //DriverManager.setLogWriter(new PrintWriter(System.out, true));
            //Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception ignore){ }
    }

    @Override
    public void desconectar() {
        try {
            //Desconectar a mysql
            conexion.close();
        }catch (SQLException ex) {
            System.out.println("Error al desconectar de la BBDD");
            //Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception ignore){}
    }

    @Override
    public void save(Ong o){
        this.conectar(); 
        try {
            util.createTables();
            
        } catch (SQLException ex) {
            System.out.println("Error creando estructuras de tablas");
            //Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        //util.savePersonas(o);
        util.saveProyectos(o);
        util.saveOng(o);

        this.desconectar();
    }

    @Override
    public void load() {
        this.conectar();
        try {
            DatabaseMetaData dbm = conexion.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "ong", null);
            if (tables.next()) {
                util.loadBBDD(ong);
                System.out.println("Datos cargados desde la BBDD");
            }
            else {
                System.out.println("No existen las tablas, debe cargar desde XML");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.desconectar();
    }
   


    
}
