/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import model.Proyecto;
import model.SaveProyecto;

/**
 *
 * @author Darkty
 */
public class MenuModificarProyecto implements Initializable{
    
    public static Proyecto proyectoZ;

    @FXML private  int codigo_proyecto;
    @FXML private DatePicker fecha_inicio;
    @FXML private DatePicker fecha_finalizacion;
    //@FXML private TextField fecha_inicio2;
    //@FXML private TextField fecha_finalizacion2;
    @FXML private TextField financiacion_aportada;
    @FXML private TextField localizacion;
    @FXML private TextField pais;
    @FXML private Button boton_guardar;
    @FXML private ComboBox linea_de_accion;
    @FXML private ComboBox sublinea_de_accion;
    
    private StringConverter converter;
    
    
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        boton_guardar.setDisable(false);
        
        linea_de_accion.getItems().add("Cooperacion_al_Desarrollo");
        linea_de_accion.getItems().add("Accion_Humanitaria");
        linea_de_accion.getItems().add("Fortalecimiento_Institucional");
        linea_de_accion.getItems().add("Educacion_para_el_Desarrollo");
        
        sublinea_de_accion.getItems().add("Promocion_Social");
        sublinea_de_accion.getItems().add("Calidad");
        sublinea_de_accion.getItems().add("Universalizacion");
        sublinea_de_accion.getItems().add("Tecnica");
        
        
        financiacion_aportada.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    financiacion_aportada.setText(oldValue);
                }
            }
        });
       

        //DATAPICKER
        fecha_inicio.setConverter(new StringConverter<LocalDate>() {
        String pattern = "dd/MM/yyyy";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            {
                fecha_inicio.setPromptText(pattern.toLowerCase());
            }

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
       });
        
        
        //DATAPICKER2
        fecha_finalizacion.setConverter(new StringConverter<LocalDate>() {
        String pattern = "dd/MM/yyyy";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
        {
        fecha_finalizacion.setPromptText(pattern.toLowerCase());
        }

        @Override public String toString(LocalDate date) {
            if (date != null) {
                return dateFormatter.format(date);
            } else {
                return "";
            }
        }

        @Override public LocalDate fromString(String string) {
            if (string != null && !string.isEmpty()) {
                return LocalDate.parse(string, dateFormatter);
            } else {
                return null;
            }
        }
       });
    }
              
    
    
    public void setProyecto(Proyecto proyecto){
        
        this.proyectoZ = proyecto;
        this.codigo_proyecto = proyecto.getCodigo_proyecto();

        LocalDate fecha_inicio_localdate = proyecto.getFecha_inicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate fecha_finalizacion_localdate = proyecto.getFecha_finalizacion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        this.fecha_inicio.setValue(fecha_inicio_localdate);
        this.fecha_finalizacion.setValue(fecha_finalizacion_localdate);
        this.financiacion_aportada.setText(String.valueOf(proyecto.getFinanciacion_aportada()));
        this.localizacion.setText(proyecto.getLocalizacion());
        this.pais.setText(proyecto.getPais());
        this.linea_de_accion.setValue(proyecto.getLinea_de_accion());
        this.sublinea_de_accion.setValue(proyecto.getSublinea_de_accion());
        
    }
    
    
    public void guardarProyectoModificar() throws IOException{
        SaveProyecto.guardarProyecto(proyectoZ, codigo_proyecto, fecha_inicio, fecha_finalizacion, financiacion_aportada, localizacion, pais, linea_de_accion, sublinea_de_accion);

        limpiarFormularioModificar();
    }
    
    
     public void limpiarFormularioModificar(){
         
        LocalDate fecha_inicio_localdate = proyectoZ.getFecha_inicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate fecha_finalizacion_localdate = proyectoZ.getFecha_finalizacion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        
        fecha_inicio.setValue(fecha_inicio_localdate);
        fecha_finalizacion.setValue(fecha_finalizacion_localdate);
        linea_de_accion.setValue(proyectoZ.getLinea_de_accion());
        sublinea_de_accion.setValue(proyectoZ.getSublinea_de_accion());
        financiacion_aportada.setText(String.valueOf(proyectoZ.getFinanciacion_aportada()));
        localizacion.setText(proyectoZ.getLocalizacion());
        pais.setText(proyectoZ.getPais());
        boton_guardar.setDisable(false);
        
    }
    
}
