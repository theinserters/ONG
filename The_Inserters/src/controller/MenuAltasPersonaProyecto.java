/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Persona;
import model.Proyecto;
import model.QueryHibernate;

/**
 *
 * @author Darkty
 */
public class MenuAltasPersonaProyecto {
    
    private Persona persona;
    private Proyecto proyecto;
    
    @FXML private TextField dni_persona;
    @FXML private TextField codigo_proyecto;
    
    
    @FXML
    public void initialize() throws SQLException {}
    
    public void setPersonaProyecto(Persona persona, Proyecto proyecto){
        dni_persona.setText(persona.getDni());
        codigo_proyecto.setText(String.valueOf(proyecto.getCodigo_proyecto()));
        this.persona = persona;
        this.proyecto = proyecto;
        
    }
    public void guardarPersonaProyecto(){
        QueryHibernate query = new QueryHibernate();
        query.savePerPro(this.proyecto, this.persona);
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Asignaci�n creada");
        alert.setHeaderText(null);
        alert.setContentText("Se ha creado la asignaci�n con �xito.");
        alert.showAndWait();
        
    } 
    
    public void limpiarPersonaProyecto(){
        dni_persona.setText("");
        codigo_proyecto.setText("");
    }
    
}
