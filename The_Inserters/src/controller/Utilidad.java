
package controller;

import model.CRUD;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Colaborador;
import model.Ong;
import model.Persona;
import model.Proyecto;
import model.Socio;
import model.Trabajador;
import model.Voluntario;


public class Utilidad {
    
    private static Connection conexion;
    
    private static final String tabla_persona = "CREATE TABLE IF NOT EXISTS persona"+
                                                "(" +
                                                " id LONG AUTO_INCREMENT NOT NULL," +
                                                " dni VARCHAR(255) NOT NULL," +
                                                " nombre VARCHAR(255)," +
                                                " apellidos VARCHAR(255)," +
                                                " tipo VARCHAR(255)," +
                                                " PRIMARY KEY (id), UNIQUE (dni) " +
                                                " )";
    
    private static final String tabla_proyecto_persona ="CREATE TABLE IF NOT EXISTS proyecto_persona" +
                                                        "(" +
                                                        " codigo_proyecto INT," +
                                                        " id_persona VARCHAR(12)" +
                                                        " )";
    
    private static final String tabla_proyecto ="CREATE TABLE IF NOT EXISTS proyecto"+
                                                "(" +
                                                " codigo_proyecto INT NOT NULL AUTO_INCREMENT," +
                                                " pais VARCHAR(128)," +
                                                " localizacion VARCHAR(128)," +
                                                " linea_de_accion ENUM('Cooperacion_al_Desarrollo', 'Accion_Humanitaria', 'Fortalecimiento_Institucional', 'Educacion_para_el_Desarrollo')," +
                                                " sublinea_de_accion ENUM('Promocion_Social', 'Calidad', 'Universalizacion', 'Tecnica')," +
                                                " fecha_inicio DATE," +
                                                " fecha_finalizacion DATE," +
                                                " socio_local TEXT," +
                                                " financiador TEXT," +
                                                " financiacion_aportada DOUBLE," +
                                                " acciones_a_realizar TEXT," +
                                                " PRIMARY KEY (codigo_proyecto)" +
                                                " )";
    
    private static final String tabla_ong = "CREATE TABLE IF NOT EXISTS ong" +
                                            "(" +
                                            " id INT AUTO_INCREMENT, " +
                                            " codigo_proyecto INT," +
                                            " ingresos DOUBLE," +
                                            " id_persona VARCHAR(12)," +
                                            " nombre VARCHAR(24)," +
                                            " capital DOUBLE," +
                                            " PRIMARY KEY (id)," +
                                            " FOREIGN KEY (codigo_proyecto) REFERENCES proyecto (codigo_proyecto) ON UPDATE CASCADE," +
                                            " FOREIGN KEY (id_persona) REFERENCES persona (id) ON UPDATE CASCADE" +
                                            ")";
    
    private static final String dropsTablas= "DROP TABLE IF EXISTS ong, persona, proyecto, proyecto_persona ";
    
    private static final String dropProcedurePersona = "DROP PROCEDURE IF EXISTS persona_dni_repetido";
    private static final String procedurePersona="Create Procedure persona_dni_repetido(id VARCHAR(12), dni VARCHAR(255), nombre VARCHAR(255), apellidos VARCHAR(255), tipo VARCHAR(255), INOUT mensaje VARCHAR(255) ) " +
                                            "BEGIN " +
                                            "	DECLARE errorcito boolean; " +
                                            "	DECLARE continue handler for sqlexception set errorcito := true; " +
                                            "	BEGIN " +
                                            "		ROLLBACK; " +
                                            "	END; " +
                                            "START TRANSACTION; " +
                                            "BEGIN " +
                                            "INSERT INTO persona (id, dni, nombre, apellidos, tipo) VALUES (id, dni, nombre, apellidos, tipo); " +
                                            "END; " +
                                            "COMMIT; " +
                                            "if errorcito then " +
                                            "	begin " +
                                            "		SET mensaje := 'Clave duplicada'; " +
                                            "	end; " +
                                            "else " +
                                            "	begin " +
                                            "		SET mensaje := 'grabado'; " +
                                            "	end; " +
                                            "end if; " +
                                            "END";
    //private static String dropProcedureProyecto = "DROP PROCEDURE IF EXISTS proyecto_repetido";
    /*
    private static String procedureProyecto = "Create Procedure proyecto_repetido(id INT, codigo_proyecto INT,pais VARCHAR(128),localizacion VARCHAR(128),linea_de_accion VARCHAR(255),sublinea_de_accion VARCHAR(255),fecha_inicio DATE,fecha_finalizacion DATE,socio_local TEXT,financiador TEXT,financiacion_aportada DOUBLE,acciones_a_realizar TEXT, INOUT mensaje VARCHAR(255)) " +
                                                " BEGIN " +
                                                "	DECLARE errorcito boolean; " +
                                                "	DECLARE continue handler for sqlexception set errorcito := true; " +
                                                "	BEGIN " +
                                                "		ROLLBACK; " +
                                                "	END; " +
                                                " START TRANSACTION; " +
                                                " BEGIN " +
                                                " INSERT INTO proyecto (codigo_proyecto,pais,localizacion,linea_de_accion,sublinea_de_accion,fecha_inicio,fecha_finalizacion,socio_local,financiador,financiacion_aportada,acciones_a_realizar) VALUES (codigo_proyecto,pais,localizacion,linea_de_accion,sublinea_de_accion,fecha_inicio,fecha_finalizacion,socio_local,financiador,financiacion_aportada,acciones_a_realizar); " +
                                                " END; " +
                                                " COMMIT; " +
                                                " if errorcito then " +
                                                "	begin " +
                                                "		SET mensaje := 'Clave duplicada'; " +
                                                "	end; " +
                                                " else " +
                                                "	begin" +
                                                "		SET mensaje := 'grabado';" +
                                                "	end;" +
                                                " end if;" +
                                                " END";

    */
    
    public Utilidad(Connection con){
        conexion = con;
    }
    
    public void savePersonas(Ong o) {
        
        try {
            //Guardar personas en BBDD
            for ( int i = 0 ; i < o.getPersonas().size() ; i++){
            //System.out.println(o.getPersonas().get(i));
            //PreparedStatement statement = conexion.prepareStatement("INSERT INTO persona (id, dni, nombre, apellidos, tipo) VALUES (?, ?, ?, ?, ?)");
            CallableStatement statement = conexion.prepareCall("{call persona_dni_repetido(?,?,?,?,?,?)}");
            
            if (o.getPersonas().get(i).getClass() == Voluntario.class){
            Voluntario v = (Voluntario)o.getPersonas().get(i);
            statement.setString(1, v.getNumVoluntario());
            statement.setString(2,  o.getPersonas().get(i).getDni());
            statement.setString(3, o.getPersonas().get(i).getNombre());
            statement.setString(4, o.getPersonas().get(i).getApellidos());
            statement.setString(5, "Voluntario");
            }
            else if (o.getPersonas().get(i).getClass() == Trabajador.class){
            Trabajador t = (Trabajador)o.getPersonas().get(i);
            statement.setString(1, t.getNumTrabajador());
            statement.setString(2,  o.getPersonas().get(i).getDni());
            statement.setString(3, o.getPersonas().get(i).getNombre());
            statement.setString(4, o.getPersonas().get(i).getApellidos());
            statement.setString(5, "Trabajador");
            }
            else if (o.getPersonas().get(i).getClass() == Colaborador.class){
            Colaborador c = (Colaborador)o.getPersonas().get(i);
            statement.setString(1, c.getNumColaborador());
            statement.setString(2,  o.getPersonas().get(i).getDni());
            statement.setString(3, o.getPersonas().get(i).getNombre());
            statement.setString(4, o.getPersonas().get(i).getApellidos());
            statement.setString(5, "Colaborador");
            }
            else if (o.getPersonas().get(i).getClass() == Socio.class){
            Socio c = (Socio)o.getPersonas().get(i);
            statement.setString(1, c.getNumSocio());
            statement.setString(2,  o.getPersonas().get(i).getDni());
            statement.setString(3, o.getPersonas().get(i).getNombre());
            statement.setString(4, o.getPersonas().get(i).getApellidos());
            statement.setString(5, "Socio");
            }
            statement.registerOutParameter(6, java.sql.Types.VARCHAR);
            statement.execute();
            //System.out.println(statement.getString(6));
            
            }
            //conexion.commit();
        }catch (SQLException ex) { //Esta excepción es mas general
            System.out.println("Error al salvar las personas en la BBDD");
            System.out.println(ex.getMessage());
            //Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void saveProyectos(Ong o) {
        
        try {
            //Guardar personas en BBDD
            for ( int i = 0 ; i < o.getProyectos().size() ; i++){
            //System.out.println(o.getPersonas().get(i));
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO proyecto (codigo_proyecto,pais,localizacion,linea_de_accion,sublinea_de_accion,fecha_inicio,fecha_finalizacion,socio_local,financiador,financiacion_aportada,acciones_a_realizar) VALUES (default,?,?,?,?,?,?,?,?,?,?);",Statement.RETURN_GENERATED_KEYS);
            //CallableStatement statement = conexion.prepareCall("{call proyecto_repetido(default,?,?,?,?,?,?,?,?,?,?,?,?)}", Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, o.getProyectos().get(i).getPais());
            statement.setString(2, o.getProyectos().get(i).getLocalizacion());
            statement.setString(3, o.getProyectos().get(i).getLinea_de_accion().toString());
            statement.setString(4, o.getProyectos().get(i).getSublinea_de_accion().toString());
            
            java.sql.Date sqlDate = java.sql.Date.valueOf(o.getProyectos().get(i).getFecha_inicio().toInstant().atZone( ZoneId.of( "Europe/Madrid" ) ).toLocalDate());
            statement.setDate(5, sqlDate);
            
            java.sql.Date sqlDateF = java.sql.Date.valueOf(o.getProyectos().get(i).getFecha_finalizacion().toInstant().atZone( ZoneId.of( "Europe/Madrid" ) ).toLocalDate());
            statement.setDate(6, sqlDateF);

            statement.setString(7, o.getProyectos().get(i).getSocio_local().toString());
            statement.setString(8, o.getProyectos().get(i).getFinanciador().toString());
            statement.setDouble(9, o.getProyectos().get(i).getFinanciacion_aportada());
            statement.setString(10, o.getProyectos().get(i).getAcciones_a_realizar().toString());
            
//            for(int z=0; z<o.getProyectos().get(i).getPersonal_voluntarios_asignados().size(); z++){
//                PreparedStatement statement2 = conexion.prepareStatement("INSERT INTO proyecto_persona ( codigo_proyecto, id_persona) VALUES (?, ?)");
//                statement2.setInt(1, o.getProyectos().get(i).getCodigo_proyecto());
//                Voluntario v = (Voluntario)o.getProyectos().get(i).getPersonal_voluntarios_asignados().get(z);
//                //statement2.setLong(2, v.getNumVoluntario()); //
//                statement2.execute();
//            } //comentado oscar
            
            statement.execute();

            }
            //conexion.commit();
        }catch (SQLException ex) { //Esta excepción es mas general
            System.out.println("Error al salvar los proyectos en la BBDD");
            System.out.println(ex.getMessage());
            //Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    public void saveOng(Ong o){
        try {

            for (int i=0; i<o.getProyectos().size(); i++){
                PreparedStatement statement;
                statement = conexion.prepareStatement("INSERT INTO ong (id, codigo_proyecto, ingresos, id_persona, nombre, capital) VALUES (default, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setInt(1, o.getProyectos().get(i).getCodigo_proyecto());
                statement.setInt(2, 0);

                for (int j=0; j < o.getPersonas().size(); j++){
                    FactoryPersona factoryPersona = new FactoryPersona();
                    CRUD<Persona> dao = factoryPersona.getPersona(o.getPersonas().get(j));
                    String id_persona = dao.recuperarCodigo(o.getPersonas().get(j));
                    statement.setString(3, id_persona);
                    statement.setString(4, o.getNombre());
                    statement.setDouble(5, o.getCapital());
                    statement.execute();
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Utilidad.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    
    public void createTables() throws SQLException{
        Statement statement = conexion.createStatement();
        statement.executeUpdate(dropsTablas);
        statement.executeUpdate(tabla_proyecto);
        statement.executeUpdate(tabla_persona);
        statement.executeUpdate(tabla_proyecto_persona);
        statement.executeUpdate(tabla_ong);
        statement.executeUpdate(dropProcedurePersona);
        statement.executeUpdate(procedurePersona);
        //statement.executeUpdate(dropProcedureProyecto);
        //statement.executeUpdate(procedureProyecto);
    }
    
    
    public void loadBBDD(Ong o) throws SQLException, ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd");
        PreparedStatement statement = conexion.prepareStatement("SELECT * FROM proyecto");
        statement.executeQuery();
        ResultSet result = statement.getResultSet();
        ArrayList<Proyecto> p = new ArrayList<Proyecto>();
        while(result.next()){
            
            Proyecto pro = new Proyecto(result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    Proyecto.LineaAccion.valueOf(result.getString(4)),
                    Proyecto.SubLineaAccion.valueOf(result.getString(5)),
                    dateFormat.format(dateParse.parse(result.getString(6))),
                    dateFormat.format(dateParse.parse(result.getString(7))));
            p.add(pro);
        }
        o.setProyectos(p);
        
        statement = conexion.prepareStatement("SELECT * FROM persona");
        statement.executeQuery();
        result = statement.getResultSet();
        ArrayList<Persona> per = new ArrayList<Persona>();
        while(result.next()){
            if ("Colaborador".equals(result.getString(5))){ 
                per.add(new Colaborador(result.getString(2),result.getString(3),result.getString(4)));
            }
            else if ("Voluntario".equals(result.getString(5))){
                per.add(new Voluntario(result.getString(2),result.getString(3),result.getString(4)));
                
            }
            else if ("Socio".equals(result.getString(5))){ 
                per.add(new Socio(result.getString(2),result.getString(3),result.getString(4)));
            }
            else if ("Trabajador".equals(result.getString(5))){ 
                per.add(new Trabajador(result.getString(2),result.getString(3),result.getString(4)));
            }
        }
        o.setPersonas(per);
        
        statement = conexion.prepareStatement("SELECT DISTINCTROW nombre, capital FROM ong");
        statement.executeQuery();
        result = statement.getResultSet();
        while(result.next()){
            o.setNombre(result.getString(1));
            o.setCapital(result.getFloat(2));
        }
        //System.out.println(per.get(0).getDni());
    }

    
}
