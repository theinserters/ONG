/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;
import model.Proyecto;
import model.QueryHibernate;
import org.hibernate.Session;

/**
 *
 * @author Darkty
 */
public class MenuAltasProyecto {
    @FXML private DatePicker fecha_inicio;
    @FXML private DatePicker fecha_finalizacion;
    //@FXML private TextField fecha_inicio2;
    //@FXML private TextField fecha_finalizacion2;
    @FXML private TextField financiacion_aportada;
    @FXML private TextField localizacion;
    @FXML private TextField pais;
    @FXML private Button boton_guardar;
    @FXML private ComboBox linea_de_accion;
    @FXML private ComboBox sublinea_de_accion;
    
    private StringConverter converter;
    
    @FXML
    public void initialize() {
        boton_guardar.setDisable(false);
        
        
        linea_de_accion.getItems().add("Cooperacion_al_Desarrollo");
        linea_de_accion.getItems().add("Accion_Humanitaria");
        linea_de_accion.getItems().add("Fortalecimiento_Institucional");
        linea_de_accion.getItems().add("Educacion_para_el_Desarrollo");
        
        sublinea_de_accion.getItems().add("Promocion_Social");
        sublinea_de_accion.getItems().add("Calidad");
        sublinea_de_accion.getItems().add("Universalizacion");
        sublinea_de_accion.getItems().add("Tecnica");
        
        financiacion_aportada.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    financiacion_aportada.setText(oldValue);
                }
            }
        });
       

        //DATAPICKER
        fecha_inicio.setConverter(new StringConverter<LocalDate>() {
        String pattern = "dd/MM/yyyy";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

        {
            fecha_inicio.setPromptText(pattern.toLowerCase());
        }

        @Override public String toString(LocalDate date) {
            if (date != null) {
                return dateFormatter.format(date);
            } else {
                return "";
            }
        }

        @Override public LocalDate fromString(String string) {
            if (string != null && !string.isEmpty()) {
                return LocalDate.parse(string, dateFormatter);
            } else {
                return null;
            }
        }
       });
        
        //DATAPICKER2
        fecha_finalizacion.setConverter(new StringConverter<LocalDate>() {
        String pattern = "dd/MM/yyyy";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
        {
        fecha_finalizacion.setPromptText(pattern.toLowerCase());
        }

        @Override public String toString(LocalDate date) {
            if (date != null) {
                return dateFormatter.format(date);
            } else {
                return "";
            }
        }

        @Override public LocalDate fromString(String string) {
            if (string != null && !string.isEmpty()) {
                return LocalDate.parse(string, dateFormatter);
            } else {
                return null;
            }
        }
       });
    }
    
    
    public void GuardarProyecto(MouseEvent event) throws IOException{
        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Proyecto p = new Proyecto();
        System.out.println("->" + financiacion_aportada.getText()+ "<-");
        try {
                if (fecha_inicio.getValue()== null){
                    p.setFecha_inicio(null);
                }else{
                    p.setFecha_inicio(dateFormat.parse(fecha_inicio.getValue().toString()));
                }
                if (fecha_finalizacion.getValue()== null){
                    p.setFecha_finalizacion(null);
                }else{
                    p.setFecha_finalizacion(dateFormat.parse(fecha_finalizacion.getValue().toString()));
                }
                if ( financiacion_aportada.getText() == null || financiacion_aportada.getText() == "" || financiacion_aportada.getText().trim().isEmpty()){
                    p.setFinanciacion_aportada(0);
                }else{
                    p.setFinanciacion_aportada(Float.parseFloat(financiacion_aportada.getText())+0);
                }
                if (linea_de_accion.getValue() == null){
                    p.setLinea_de_accion(null);
                }else{
                    p.setLinea_de_accion(Proyecto.LineaAccion.valueOf(linea_de_accion.getValue().toString()));
                }
                if (sublinea_de_accion.getValue() == null){
                    p.setSublinea_de_accion(null);
                }else{
                    p.setSublinea_de_accion(Proyecto.SubLineaAccion.valueOf(sublinea_de_accion.getValue().toString()));
                }
                    
                p.setLocalizacion(localizacion.getText());
                p.setPais(pais.getText());
                Session session = QueryHibernate.getSession();
                session.beginTransaction();
                session.save(p);
                session.getTransaction().commit();
                session.close();
                p = null;
 
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Alta Proyecto");
                alert.setHeaderText(null);
                alert.setContentText("Datos salvados correctamente.");
                alert.showAndWait();
                LimpiarFormulario();
    
            } catch (ParseException ex) {
            System.out.println("El formato de las fechas es err�neo!");
            //Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public void LimpiarFormulario(){
        fecha_inicio.setValue(null);
        linea_de_accion.setValue("");
        sublinea_de_accion.setValue("");
        fecha_finalizacion.setValue(null);
        financiacion_aportada.setText("");
        localizacion.setText("");
        pais.setText("");
        boton_guardar.setDisable(false);
        fecha_inicio.setFocusTraversable(true);
        
    }
}
