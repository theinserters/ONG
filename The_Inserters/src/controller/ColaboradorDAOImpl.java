
package controller;

import model.ColaboradorDAO;
import model.Colaborador;
import model.Persona;
import model.Trabajador;


public class ColaboradorDAOImpl implements ColaboradorDAO {
    
    @Override
    public void imprimirDatos(Persona t) {
        //System.out.println("Colaborador: "+t.toString());
        Colaborador o = (Colaborador)t;
        System.out.println("Colaborador: "+ o.getNumColaborador()+" - "+o.getDni());
    }

    @Override
    public String recuperarCodigo(Persona t){
        Colaborador v = (Colaborador)t;
        return v.getNumColaborador();
    }
    
}
