
package controller;

import java.io.IOException;
import java.sql.SQLException;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import model.Colaborador;
import model.QueryHibernate;
import model.SavePersona;
import model.Socio;
import model.Trabajador;
import model.Voluntario;


public class MenuAltasPersona {
    @FXML private ComboBox tipo;
    @FXML private TextField id_persona;
    @FXML private TextField dni;
    @FXML private TextField apellidos;
    @FXML private TextField nombre;
    @FXML private Label etiqueta_repetido;
    @FXML private Button boton_guardar;
    @FXML private CheckBox pagado;
    
    @FXML
    public void initialize() throws SQLException {

        tipo.getItems().add("Colaborador");
        tipo.getItems().add("Socio");
        tipo.getItems().add("Trabajador");
        tipo.getItems().add("Voluntario");
        
        pagado.setSelected(false);

       
         dni.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) -> {
             //Controlamos cuando dimX pierde el foco
             if(!arg2){
                 //Aqu� va el c�digo que queremos que se ejecute cuando dimX pierda el foco.
                 DNIUpdated();
             }
        });
        
        //pregunta si la tabla existe
        
        //fin pregunta si la tabla existe
    }
    
    
    public void ComboUpdated(){ 
        Long count = QueryHibernate.comboUpdate(tipo);
        if (tipo.getValue()== "Voluntario"){
           this.id_persona.setText("V" + (count + 1));
           pagado.setVisible(false);
           Voluntario p = new Voluntario();
        }else if (tipo.getValue()== "Colaborador"){
           this.id_persona.setText("C" + (count + 1));
           pagado.setVisible(false);
           Colaborador p = new Colaborador();
        }else if (tipo.getValue()== "Socio"){
           this.id_persona.setText("S" + (count + 1));
           pagado.setVisible(true);
           Socio p = new Socio();
        }else if (tipo.getValue()== "Trabajador"){
           this.id_persona.setText("T" + (count + 1));
           pagado.setVisible(false);
           Trabajador p = new Trabajador();
        }
    }
    
    
    public void DNIUpdated(){
        Long count = QueryHibernate.dniUpdated(dni);
        if (count > 0) {
            etiqueta_repetido.setVisible(true);
            boton_guardar.setDisable(true);
        }else{
            etiqueta_repetido.setVisible(false);
            boton_guardar.setDisable(false);
        }
    }
    
    public void GuardarPersona(MouseEvent event) throws IOException{
        SavePersona.guardarPersona(id_persona, tipo, dni, apellidos, nombre, pagado);

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Alta Persona");
        alert.setHeaderText(null);
        alert.setContentText("Datos salvados correctamente.");
        alert.showAndWait();
        LimpiarFormulario();
    }
    
    public void LimpiarFormulario(){
        id_persona.setText("");
        tipo.setValue("");
        //tipo.getItems().clear();
        dni.setText("");
        apellidos.setText("");
        nombre.setText("");
        pagado.setSelected(false);
        pagado.setVisible(false);
        etiqueta_repetido.setVisible(false);
        boton_guardar.setDisable(true);
        tipo.setFocusTraversable(true); 
    }
    
}
