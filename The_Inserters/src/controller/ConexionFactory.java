
package controller;

import model.ConexionDAO;


public class ConexionFactory {
    
    public ConexionDAO getConexion(String motor){
        if(motor.equalsIgnoreCase("MySQL")){
            return new ConexionMySQL();
        }else if(motor.equalsIgnoreCase("XML")){
            return new ConexionXML();
        }
        return null;
    }
    
}
