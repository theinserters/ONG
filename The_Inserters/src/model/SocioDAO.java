
package model;

import model.Persona;


public interface SocioDAO extends CRUD<Persona> {
    
    public void pagarCuota();
  
}
