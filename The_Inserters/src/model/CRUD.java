
package model;

import model.Persona;


public interface CRUD<Persona> {
    
    public void imprimirDatos(Persona t);
    
    public String recuperarCodigo(Persona t);
}