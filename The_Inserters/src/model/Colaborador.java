
package model;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity(name = "Colaborador")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Colaborador") 
@XmlRootElement(name="Colaborador")
@XmlType(propOrder = {"numColaborador"})
public class Colaborador extends Persona{
    
    @Column(name = "id_persona", insertable = true, updatable = true)
    private String numColaborador = "C";
    
    private static AtomicInteger n = new AtomicInteger(1);
    
//    @Column(name = "tipo", insertable = false, updatable = false)
//    private String tipo = "Colaborador";
    
    
    public Colaborador() {
        
    }

    public Colaborador(String dni, String nombre, String apellidos) {
        super(dni, nombre, apellidos);
        this.numColaborador = numColaborador + n.getAndIncrement();
        super.setId_persona(this.numColaborador);
        super.setTipo("Colaborador");
        super.setPagado("---");
    }
    
    public Colaborador( String dni, String nombre, String apellidos, String numColaborador) {
        super(dni, nombre, apellidos);
        this.numColaborador = numColaborador;
        super.setId_persona(this.numColaborador);
        super.setTipo("Colaborador");
        super.setPagado("---");
    }

    public String getNumColaborador() {
        return numColaborador;
    }

    public void setNumColaborador(String numColaborador) {
        this.numColaborador = numColaborador;
    }
    
    @Override
    public String getTipo() {
        return tipo;
    }
    @Override
    public void setTipo(String tipo) {
        super.tipo = tipo;
    }

   
    @Override
    public String toString() {
        return "COLABORADOR: " + super.toString() + "numColaborador=" + numColaborador;
    }

    
}
