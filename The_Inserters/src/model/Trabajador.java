/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.concurrent.atomic.AtomicInteger;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity(name = "Trabajador")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Trabajador") 
@XmlRootElement(name="Trabajador")
@XmlType(propOrder = {"numTrabajador"})
public class Trabajador extends Persona{
    
    @Column(name = "id_persona", insertable = true, updatable = true)
    private String numTrabajador = "T";
    
    private static AtomicInteger n = new AtomicInteger(1);
    
//    @Column(name = "tipo", insertable = false, updatable = false)
//    private String tipo = "Trabajador";

    public Trabajador() {
    }

    public Trabajador(String dni, String nombre, String apellidos) {
        super(dni, nombre, apellidos);
        this.numTrabajador = numTrabajador + n.getAndIncrement();
        super.setId_persona(this.numTrabajador);
        super.setTipo("Trabajador");
        super.setPagado("NO");
    }
    
    public Trabajador(String dni, String nombre, String apellidos, String numTrabajador) {
        super(dni, nombre, apellidos);
        this.numTrabajador = numTrabajador;
        super.setId_persona(this.numTrabajador);
        super.setTipo("Trabajador");
        super.setPagado("NO");
    }

    public String getNumTrabajador() {
        return numTrabajador;
    }

    public void setNumTrabajador(String numTrabajador) {
        this.numTrabajador = numTrabajador;
    }
    @Override
    public String getTipo() {
        return tipo;
    }
    @Override
    public void setTipo(String tipo) {
        super.tipo = tipo;
    }

   
    @Override
    public String getPagado() {
        return pagado;
    }

    @Override
    public void setPagado(String pagado) {
        this.pagado = pagado;
    }

    
    @Override
    public String toString() {
        return "TRABAJADOR: " + super.toString() + "numTrabajador=" + numTrabajador;
    }
    
    

}
