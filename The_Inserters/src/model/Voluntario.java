
package model;

import java.util.concurrent.atomic.AtomicInteger;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity(name = "Voluntario")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Voluntario") 
@XmlRootElement(name="Voluntario")
@XmlType(propOrder = {"numVoluntario"})
public class Voluntario extends Persona{
    
    @Column(name = "id_persona", insertable = true, updatable = true)
    private String numVoluntario = "V";
    
    private static AtomicInteger n = new AtomicInteger(1);

//    @Column(name = "tipo", insertable = false, updatable = false)
//    private String tipo = "Voluntario";
    
    public Voluntario() {
        super.setTipo("Voluntario");
        super.setPagado("NO");
    }

    public Voluntario(String dni, String nombre, String apellidos) {
        super(dni, nombre, apellidos);
        this.numVoluntario = numVoluntario + n.getAndIncrement();
        super.setId_persona(this.numVoluntario);
        super.setTipo("Voluntario");
        super.setPagado("NO");
    }
    @Override
    public String getTipo() {
        return tipo;
    }
    @Override
    public void setTipo(String tipo) {
        super.tipo = tipo;
    }
    

    @Override
    public String getPagado() {
        return super.pagado;
    }

    @Override
    public void setPagado(String pagado) {
        super.pagado = pagado;
    }

    public String getNumVoluntario() {
        return numVoluntario;
    }

    public void setNumVoluntario(String numVoluntario) {
        this.numVoluntario = numVoluntario;
    }

    @Override
    public String toString() {
        return "VOLUNTARIO: " + super.toString() + "numVoluntario=" + numVoluntario;
    }
    
    
    
    
}
