
package model;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity(name = "proyecto")
@Table(name = "proyecto")
@XmlRootElement(name = "Proyecto")
@XmlType(propOrder = {"codigo_proyecto","pais","localizacion","linea_de_accion","sublinea_de_accion","fecha_inicio","fecha_finalizacion","socio_local","financiador","financiacion_aportada","acciones_a_realizar","personal_voluntarios_asignados"})
public class Proyecto{
    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo_proyecto")
    @Id
    private int codigo_proyecto;
    @Column(name="pais")
    private String pais;
    @Column(name="localizacion")
    private String localizacion;
    @Enumerated(EnumType.STRING)
    @Column(name="linea_de_accion")
    private LineaAccion linea_de_accion;
    @Enumerated(EnumType.STRING)
    @Column(name="sublinea_de_accion")
    private SubLineaAccion sublinea_de_accion;
    @Column(name="fecha_inicio")
    private Date fecha_inicio;
    @Column(name="fecha_finalizacion")
    private Date fecha_finalizacion;
    @Column(name="socio_local")
    @ElementCollection
    private List<String> socio_local = new ArrayList<String>();
    @Column(name="financiador")
    @ElementCollection
    private List<String> financiador = new ArrayList<String>();
    @Column(name="financiacion_aportada")
    private float financiacion_aportada;
    @Column(name="acciones_a_realizar")
    @ElementCollection
    private List<String> acciones_a_realizar = new ArrayList<String>();

    @Column(unique = false)
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Persona> personas;


    public enum LineaAccion{
    Cooperacion_al_Desarrollo, Accion_Humanitaria, Fortalecimiento_Institucional, Educacion_para_el_Desarrollo;
    }
    
    public enum SubLineaAccion{
    Promocion_Social, Calidad, Universalizacion, Tecnica;
    }

    
    public Proyecto() {
    }
    
    public Proyecto(String pais, String localizacion, LineaAccion la, SubLineaAccion sla, String fecha_e, String fecha_f ) {
        
        //this.codigo_proyecto = codPro;
        this.pais = pais;
        this.localizacion = localizacion;
        this.linea_de_accion = la;
        this.sublinea_de_accion = sla;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.fecha_inicio = dateFormat.parse(fecha_e);
            this.fecha_finalizacion = dateFormat.parse(fecha_f);
        } catch (ParseException ex) {
            System.out.println("El formato de las fechas es err�neo!");
            Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Proyecto(int codPro, String pais, String localizacion, LineaAccion la, SubLineaAccion sla, String fecha_e, String fecha_f) {
        
        this.codigo_proyecto = codPro;
        this.pais = pais;
        this.localizacion = localizacion;
        this.linea_de_accion = la;
        this.sublinea_de_accion = sla;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.fecha_inicio = dateFormat.parse(fecha_e);
            this.fecha_finalizacion = dateFormat.parse(fecha_f);
        } catch (ParseException ex) {
            System.out.println("El formato de las fechas es err�neo!");
            Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getCodigo_proyecto() {
        return codigo_proyecto;
    }

    public void setCodigo_proyecto(int codigo_proyecto) {
        this.codigo_proyecto = codigo_proyecto;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    //@Enumerated(EnumType.STRING)
    public LineaAccion getLinea_de_accion() {
        return linea_de_accion;
    }

    public void setLinea_de_accion(LineaAccion linea_de_accion) {
        this.linea_de_accion = linea_de_accion;
    }
    
    //@Enumerated(EnumType.STRING)
    public SubLineaAccion getSublinea_de_accion() {
        return sublinea_de_accion;
    }

    public void setSublinea_de_accion(SubLineaAccion sublinea_de_accion) {
        this.sublinea_de_accion = sublinea_de_accion;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    
    public Date getFecha_finalizacion() {
        return fecha_finalizacion;
    }

    public void setFecha_finalizacion(Date fecha_finalizacion) {
        this.fecha_finalizacion = fecha_finalizacion;
    }

    public List<String> getSocio_local() {
        return socio_local;
    }

    public void setSocio_local(List<String> socio_local) {
        this.socio_local = socio_local;
    }

    public List<String> getFinanciador() {
        return financiador;
    }

    public void setFinanciador(List<String> financiador) {
        this.financiador = financiador;
    }

    public float getFinanciacion_aportada() {
        return financiacion_aportada;
    }

    public void setFinanciacion_aportada(float financiacion_aportada) {
        this.financiacion_aportada = financiacion_aportada;
    }

    public List<String> getAcciones_a_realizar() {
        return acciones_a_realizar;
    }

    public void setAcciones_a_realizar(List<String> acciones_a_realizar) {
        this.acciones_a_realizar = acciones_a_realizar;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    @Override
    public String toString() {
        return "Proyecto{" + "codigo_proyecto=" + codigo_proyecto + ", pais=" + pais + ", localizacion=" + localizacion + ", linea_de_accion=" + linea_de_accion + ", sublinea_de_accion=" + sublinea_de_accion + ", fecha_inicio=" + fecha_inicio + ", fecha_finalizacion=" + fecha_finalizacion + ", socio_local=" + socio_local + ", financiador=" + financiador + ", financiacion_aportada=" + financiacion_aportada + ", acciones_a_realizar=" + acciones_a_realizar + ", personas=" + personas + '}';
    }
    
    
}
