package model;

import java.util.concurrent.atomic.AtomicInteger;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@Entity(name = "Socio")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Socio") 
@XmlRootElement(name="Socio")
@XmlType(propOrder = {"numSocio","pagado"})
public class Socio extends Persona{
    
    @Column(name = "id_persona", insertable = true, updatable = true)
    private String numSocio = "S";
    
    private static AtomicInteger n = new AtomicInteger(1);

    private String pagado;
    
//    @Column(name = "tipo", insertable = false, updatable = false)
//    private String tipo = "Socio";
    
    public Socio() {
    }
    
    public Socio(String dni, String nombre, String apellidos) {
        super(dni, nombre, apellidos);
        this.numSocio = numSocio + n.getAndIncrement();
        super.setId_persona(this.numSocio);
        super.setTipo("Socio");
        super.setPagado("No");
    }
    
    public Socio(String dni, String nombre, String apellidos, String numSocio, String pagado) {
        super(dni, nombre, apellidos);
        this.numSocio = numSocio;
        this.pagado = pagado;
        super.setId_persona(this.numSocio);
        super.setTipo("Socio");
        super.setPagado("No");

       
    }
    
    @XmlElement(name = "numSocio")
    public String getNumSocio(){
        return this.numSocio;
    }
    
    public String getPagado() {
        return pagado;
    }

    @Override
    public void setPagado(String pagado) {
        this.pagado = pagado;
        super.pagado = pagado;
    }

    public void setNumSocio(String numSocio) {
        this.numSocio = numSocio;
    }
    @Override
    public String getTipo() {
        return tipo;
    }
    @Override
    public void setTipo(String tipo) {
       super.tipo = tipo;
    }

  
    
    
    @Override
    public String toString() {
        return "SOCIO: " + super.toString() + "numSocio=" + numSocio + ", pagado=" + pagado;
    }
    
    
    
  
    
}
