
package model;



public interface ConexionDAO {
    
    void conectar();
    void desconectar();
    
    void save(Ong o);
    void load();
    
}
