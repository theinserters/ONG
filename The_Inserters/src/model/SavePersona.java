/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static controller.MenuModificarPersona.personaM;
import java.io.IOException;
import java.util.List;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 *
 * @author javier
 */
public class SavePersona {
    
    //private QueryHibernate query = new QueryHibernate();
    
   
    public static void guardarPersona(Persona persona, TextField id_persona, ComboBox tipo, TextField dni, TextField apellidos, TextField nombre, CheckBox pagado) throws IOException{
        
        if (tipo.getValue()=="Voluntario"){
            Session session = QueryHibernate.getSession();
            Voluntario per = new Voluntario();
            per.setId(persona.getId());
            id_persona.setDisable(false);
            per.setNumVoluntario(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }else{
                persona.setPagado("");
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            session.beginTransaction();
            session.delete(per);
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else if (tipo.getValue()== "Colaborador"){
            Session session = QueryHibernate.getSession();
            Colaborador per = new Colaborador();
            per.setId(persona.getId());
            id_persona.setDisable(false);
            per.setNumColaborador(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            session.beginTransaction();
            session.delete(per);
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else if (tipo.getValue()== "Socio"){
            Session session = QueryHibernate.getSession();
            Socio per = new Socio();
            per.setId(persona.getId());
            id_persona.setDisable(false);
            per.setNumSocio(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            session.beginTransaction();
            session.delete(per);
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else if (tipo.getValue()== "Trabajador"){
            Session session = QueryHibernate.getSession();
            Trabajador per = new Trabajador();
            per.setId(persona.getId());
            id_persona.setDisable(false);
            per.setNumTrabajador(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            session.beginTransaction();
            session.delete(per);
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }
    }
    
    
    public static void guardarPersona( TextField id_persona, ComboBox tipo, TextField dni, TextField apellidos, TextField nombre, CheckBox pagado) throws IOException{
        
        
        if (tipo.getValue()== "Voluntario"){
            Voluntario per = new Voluntario();
            id_persona.setDisable(false);
            per.setNumVoluntario(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            Session session = QueryHibernate.getSession();
            session.beginTransaction();
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else if (tipo.getValue()== "Colaborador"){
            Colaborador per = new Colaborador();
            id_persona.setDisable(false);
            per.setNumColaborador(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            Session session = QueryHibernate.getSession();
            session.beginTransaction();
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else if (tipo.getValue()== "Socio"){
            Socio per = new Socio();
            id_persona.setDisable(false);
            per.setNumSocio(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            Session session = QueryHibernate.getSession();
            session.beginTransaction();
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }else{
            Trabajador per = new Trabajador();
            id_persona.setDisable(false);
            per.setNumTrabajador(id_persona.getText());
            id_persona.setDisable(true);
            per.setTipo((String) tipo.getValue());
            per.setDni(dni.getText());
            if (tipo.getValue() == "Socio" ){
                if (pagado.isSelected()){
                    per.setPagado("SI");
                }else{
                    per.setPagado("NO");
                }
            }
            per.setApellidos(apellidos.getText());
            per.setNombre(nombre.getText());
            Session session = QueryHibernate.getSession();
            session.beginTransaction();
            session.save(per);
            session.getTransaction().commit();
            session.close();
            per = null;
        }
        
        
    }
    
}
