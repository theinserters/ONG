
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name = "proyecto_persona")
@Table(name = "proyecto_persona")
public class Proyecto_Persona implements Serializable{
    
    @Id
    @Column(name="proyecto_codigo_proyecto")
    private int proyecto_codigo_proyecto;
    @Id
    @Column(name="personas_id")
    private String personas_id;
    
    
    public Proyecto_Persona(){}

    public Proyecto_Persona(int proyectos_codigo_proyecto, String personas_dni) {
        this.proyecto_codigo_proyecto = proyectos_codigo_proyecto;
        this.personas_id = personas_dni;
    }
//
    public int getProyectos_codigo_proyecto() {
        return proyecto_codigo_proyecto;
    }

    public int getProyecto_codigo_proyecto() {
        return proyecto_codigo_proyecto;
    }

    public void setProyecto_codigo_proyecto(int proyecto_codigo_proyecto) {
        this.proyecto_codigo_proyecto = proyecto_codigo_proyecto;
    }

    public String getPersonas_id() {
        return personas_id;
    }

    public void setPersonas_id(String personas_id) {
        this.personas_id = personas_id;
    }

}
