
package model;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.hibernate.Session;


public class SaveProyecto {
    

    public static void guardarProyecto(Proyecto p, int codigo_proyecto, DatePicker fecha_inicio, DatePicker fecha_finalizacion, TextField financiacion_aportada, TextField localizacion, TextField pais, ComboBox linea_de_accion, ComboBox sublinea_de_accion) throws IOException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //System.out.println("->" + financiacion_aportada.getText()+ "<-");
        try {
                if (fecha_inicio.getValue()== null){
                    p.setFecha_inicio(null);
                }else{
                    p.setFecha_inicio(dateFormat.parse(fecha_inicio.getValue().toString()));
                }
                if (fecha_finalizacion.getValue()== null){
                    p.setFecha_finalizacion(null);
                }else{
                    p.setFecha_finalizacion(dateFormat.parse(fecha_finalizacion.getValue().toString()));
                }
                if ( financiacion_aportada.getText() == null || financiacion_aportada.getText().equalsIgnoreCase("") || financiacion_aportada.getText().trim().isEmpty()){
                    p.setFinanciacion_aportada(0);
                }else{
                    p.setFinanciacion_aportada(Float.parseFloat(financiacion_aportada.getText())+0);
                }
                if (linea_de_accion.getValue() == null){
                    p.setLinea_de_accion(null);
                }else{
                    p.setLinea_de_accion(Proyecto.LineaAccion.valueOf(linea_de_accion.getValue().toString()));
                }
                if (sublinea_de_accion.getValue() == null){
                    p.setSublinea_de_accion(null);
                }else{
                    p.setSublinea_de_accion(Proyecto.SubLineaAccion.valueOf(sublinea_de_accion.getValue().toString()));
                }
                
                p.setLocalizacion(localizacion.getText());
                p.setPais(pais.getText());

                Session session = QueryHibernate.getSession();
                session.beginTransaction();
                session.saveOrUpdate(p);
                session.getTransaction().commit();
                session.close();
                
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Modificar Proyecto");
                alert.setHeaderText(null);
                alert.setContentText("Datos salvados correctamente.");
                alert.showAndWait();
    
            } catch (ParseException ex) {
            System.out.println("El formato de las fechas es err�neo!");
            //Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    
}
