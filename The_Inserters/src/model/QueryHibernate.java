
package model;

import java.util.List;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class QueryHibernate {
    
    private static StandardServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;
    
    public static Session getSession(){
        Configuration configuration = new Configuration().configure();
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.configure().buildSessionFactory(serviceRegistry);
        Session session = sessionFactory.openSession();
        return session;
    }
    
    public void saveAll(Ong ong){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        
        session.save(ong);
        
        ong.getPersonas().forEach((p) -> {
            session.save(p);
        });
        ong.getProyectos().forEach((p) -> {
            session.save(p);
        });
        
//        getPerPro().forEach((p) -> {
//            session.save(p);
//        });

        session.getTransaction().commit();
        session.close();
    }
    
    public void updatePersonas(Ong ong){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        
        ong.getPersonas().forEach((p) -> {
            session.update(p);
        });

        session.getTransaction();
        session.close();
    }
    

    public static void deleteProyecto(Proyecto proyecto){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        
        List lista = getPerProCodigo(proyecto.getCodigo_proyecto());
        for(int i =0; i< lista.size(); i++){
            session.delete(lista.get(i));
        }
        
        session.delete(proyecto);
        
        session.getTransaction().commit();
        session.close();
    }
    
    public static void deletePersona(Persona persona){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        session.delete(persona);
        //buscar aquellas personas en proyecto_persona que contienen ese id
        List lista = getPerProDNI(persona.getId_persona());
        for(int i =0; i< lista.size(); i++){
            session.delete(lista.get(i));
        }

        session.delete(persona);

        session.getTransaction().commit();
        session.close();
    }
    
    public static void deletePersonaProyecto(Proyecto_Persona personaAsignada){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        session.delete(personaAsignada);
        session.getTransaction().commit();
        session.close();
    }
    
    public List<Persona> getPersonas(){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        List<Persona> lista = session.createCriteria(Persona.class).list();

        session.getTransaction().commit();
        session.close();  

        return lista;
    }
    
    public List<Proyecto> getProyectos(){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        List<Proyecto> lista = session.createCriteria(Proyecto.class).list();

        session.getTransaction().commit();
        session.close();  

        return lista;
    }
    
    public List<Proyecto_Persona> getPerPro(){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        List lista = session.createCriteria(Proyecto_Persona.class).list();

        session.getTransaction().commit();
        session.close();
        return lista;
    }
    
    public static List<Proyecto_Persona> getPerProCodigo(int codigo){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        Query query = session.createQuery("from proyecto_persona where proyecto_codigo_proyecto = :codigo");
        query.setInteger("codigo", codigo);
        List lista = query.list();
        
        session.getTransaction().commit();
        session.close();
        return lista;
    }
    
    public static List<Proyecto_Persona> getPerProDNI(String id){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        Query query = session.createQuery("from proyecto_persona where personas_id = :id");
        query.setString("id", id);
        List lista = query.list();
        
        session.getTransaction().commit();
        session.close();
        return lista;
    }
    
    
    public void savePerPro(Proyecto proyecto, Persona persona){
        //List<Proyecto_Persona> list = new ArrayList<>();
        Proyecto_Persona pp = new Proyecto_Persona(proyecto.getCodigo_proyecto(), persona.getId_persona());
        //list.add(pp);
        
        Session session = QueryHibernate.getSession();
        session.beginTransaction();

        session.saveOrUpdate(proyecto);
        session.saveOrUpdate(persona);
        session.saveOrUpdate(pp);
        
        session.getTransaction().commit();
        session.close();  
    }
    
    public static Long dniUpdated(TextField dni){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        Query query = session.createQuery("select count(*) from persona as p where p.dni = :dnicito");
        query.setString("dnicito", dni.getText());
        Long count = (Long)query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count;
    }
    
    public static Long comboUpdate(ComboBox tipo){
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        
        Query query = session.createQuery("select count(*) from persona as p where p.tipo like :tipito");
        query.setString("tipito", tipo.getValue().toString());
        Long count = (Long)query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count;
    }
    
}


