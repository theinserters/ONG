
package model;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import model.Proyecto.LineaAccion;
import model.Proyecto.SubLineaAccion;

@Entity
@Table(name = "ong")
@XmlRootElement(name = "Ong")
@XmlSeeAlso({Socio.class, Voluntario.class, Colaborador.class, Trabajador.class, Proyecto.class})
public class Ong{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    private static AtomicInteger n = new AtomicInteger(1);
    
    @Column(name="nombre")
    private String nombre;
    @Column(name="capital")
    private double capital;
    @Transient
    private ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
    @Transient
    private ArrayList<Persona> personas = new ArrayList<Persona>();

    
    public Ong(){}

    public Ong(String nombre, double capital){
        this.nombre  = nombre;
        this.capital = capital;
        this.id = n.getAndIncrement();
    }
    
    @XmlElementWrapper(name = "Proyectos")
    @XmlElement(name = "Proyecto")
    public ArrayList<Proyecto> getProyectos() {
        return proyectos;
    }

    public void setProyectos(ArrayList<Proyecto> proyectos) {
        this.proyectos = proyectos;
    }

    @XmlElementWrapper(name = "Personas")
    @XmlElement(name = "Persona")
    public ArrayList<Persona> getPersonas() {
        return this.personas;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static AtomicInteger getN() {
        return n;
    }

    public static void setN(AtomicInteger n) {
        Ong.n = n;
    }
    
    public void crearPersona(Ong ong){
        ArrayList<Persona> lista = ong.getPersonas();
        Scanner entrada = new Scanner(System.in);
        System.out.println("Introduzca el DNI:");
        String dni = entrada.nextLine();
        if (comprobarDNI(lista, dni)){
            System.out.println("Esta persona ya existe");
        }else{
            System.out.println("Introduzca el Nombre:");
            String nombre = entrada.nextLine();
            System.out.println("Introduzca los Apellidos:");
            String apellidos = entrada.nextLine();
            String categoria = null;
            Persona p = null;
            do{
                System.out.println("Introduzca la categor�a de la persona(Voluntario, Trabajador, Socio, Colaborador):");
                categoria = entrada.next();
                
                if(categoria.equalsIgnoreCase("Voluntario")) p = new Voluntario(dni, nombre, apellidos);
                else if(categoria.equalsIgnoreCase("Trabajador")) p = new Trabajador(dni, nombre, apellidos);
                else if(categoria.equalsIgnoreCase("Socio")) p = new Socio(dni, nombre, apellidos);
                else if(categoria.equalsIgnoreCase("Colaborador")) p = new Colaborador(dni, nombre, apellidos);
                else{
                    categoria = null;
                    System.out.println("�No ha introducido una categor�a v�lida!");
                }
            }while(categoria == null);
            
            ong.personas.add(p);
            System.out.println("�Persona creada!");
            
        }
       
    }
    
    public void crearProyecto(Ong ong){
        ArrayList<Proyecto> lista = ong.getProyectos();
        Scanner entrada = new Scanner(System.in);

        int codigoPro = (ong.getProyectos().size() + 1) ;   

            System.out.println("Introduzca el Pa�s:");
            String pais = entrada.next();
            System.out.println("Introduzca la localizaci�n:");
            String localizacion = entrada.next();
            
            System.out.println("Escribe una l�nea de acci�n: ");
            for (LineaAccion la: LineaAccion.values()){
                System.out.print("\t- "+la+"\n");
            }
            String la = entrada.next();
            LineaAccion lineaA = LineaAccion.valueOf(la);
            
            System.out.println("Escribe una subl�nea de acci�n: ");
            for (SubLineaAccion sla: SubLineaAccion.values()){
                System.out.print("\t- "+sla+"\n");
            }
            String sla = entrada.next();
            SubLineaAccion subLineaA = SubLineaAccion.valueOf(sla);
            
            System.out.println("Introduzca la fecha de entrada (dd/mm/aaaa):");
            String fecha_e = entrada.next();
            
            System.out.println("Introduzca la fecha de finalizaci�n (dd/mm/aaaa):");
            String fecha_f = entrada.next();
            
            ong.proyectos.add(new Proyecto(codigoPro, pais, localizacion, lineaA, subLineaA, fecha_e, fecha_f ));
            System.out.println("�Proyecto creado!");
            
       // }
       
    }
    
    public boolean comprobarProyecto(ArrayList<Proyecto> lista, int codigo){
        for (Proyecto p: lista){
            if (codigo == p.getCodigo_proyecto()) return true;
        }
        return false;
    }
    
    public boolean comprobarDNI(ArrayList<Persona> lista, String dni){
        for (Persona p: lista){
            if(p.getDni().compareToIgnoreCase(dni) == 0) return true;
        }
        return false;
    }
    
    public void eliminarPersona(Ong ong){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el DNI de la persona a eliminar: ");
        String dni = entrada.nextLine();
        boolean encontrado = false;
        for ( Persona p : ong.getPersonas()){
            if (p.getDni().equalsIgnoreCase(dni)){
                ong.getPersonas().remove(p);
                encontrado = true;
                break;
            }
        }
        if (encontrado){
           System.out.println("Se ha eliminado la persona con DNI: " + dni);
        }
            
    }
    
    public void eliminarProyecto(Ong ong){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el c�digo del proyecto a eliminar: ");
        int codPro = entrada.nextInt();
        boolean encontrado = false;
        for ( Proyecto p : ong.getProyectos()){
            if (p.getCodigo_proyecto() == codPro ){
                ong.getProyectos().remove(p);
                encontrado = true;
                break;
            }
        }
        if (encontrado){
           System.out.println("Se ha eliminado el proyecto con c�digo: " + codPro);
        }
            
    }
    
    public void asignarPersonaProyecto(Ong ong){
        Scanner entrada = new Scanner(System.in);
        Persona persona = new Persona();
        if(ong.getPersonas().size()<=0) System.out.println("�Error: no existen personas que mostrar!");
        else{
            for (Persona p: ong.getPersonas()){
                if (p.getClass() == Voluntario.class){
                    System.out.println(p.toString());
                }
                
            }
            System.out.print("Indica el DNI de la persona a asignar: ");
            String dni = entrada.nextLine();
            
            for (int z=0; z < ong.getPersonas().size(); z++){
                if ( dni.equalsIgnoreCase(ong.getPersonas().get(z).getDni()) ){
                    persona = ong.getPersonas().get(z);
                    break;
                }
            }
            
            if(ong.getProyectos().size()<=0)System.out.println("�Error: no existen proyectos que mostrar!");
            else{
                for (Proyecto p: ong.getProyectos()) System.out.println(p.toString());
                System.out.print("Indica el c�digo del proyecto: ");
                int codPro = entrada.nextInt();
                for (int i=0; i < ong.getProyectos().size(); i++){
                    if ( codPro == ong.getProyectos().get(i).getCodigo_proyecto() ){
                        ong.getProyectos().get(i).getPersonas().add(persona); //comentado oscar
                            
                        break;
                    }
                }
                System.out.println("�Persona asignada a proyecto!");
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
