

import java.util.Iterator;
import model.Colaborador;
import model.Ong;
import model.Persona;
import model.Proyecto;
import model.Proyecto.LineaAccion;
import model.Proyecto.SubLineaAccion;
import model.QueryHibernate;
import model.Voluntario;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class HibernateTest {
 
    
    public static void main(String[] args) {
        Session session = QueryHibernate.getSession();
        session.beginTransaction();
        
        Colaborador p = new Colaborador("2366577", "Oscar", "Martinez");
        session.save(p);
        Proyecto po  = new Proyecto("Espa�a", "Madrid", LineaAccion.Accion_Humanitaria, SubLineaAccion.Calidad, "01/01/2021", "02/02/2021" );
        session.save(po);
        Ong o  = new Ong("ONG", 200.0);
        session.save(o);

        session.getTransaction().commit();
        session.close();

    }
 
}