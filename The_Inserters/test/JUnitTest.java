
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import model.Ong;
import model.Persona;
import model.Proyecto;
import org.junit.Test;
import static org.junit.Assert.assertNotNull;


public class JUnitTest {
    
    public JUnitTest() {
    }
    
    @Test
    public void CompruebaCargaFichero(){
        try { 
            JAXBContext context = JAXBContext.newInstance(Ong.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Ong o ;
            o = (Ong) unmarshaller.unmarshal(new File("ong.xml"));
            
            assertNotNull("Objeto Ong nullo!",o);
            
        } catch (JAXBException ex) {
            Logger.getLogger(JUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void compruebaPersonas() throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(Ong.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Ong o ;
        o = (Ong) unmarshaller.unmarshal(new File("ong.xml"));
        
        for ( Persona p : o.getPersonas()){
            assertNotNull("Persona nula!", p);
        }
    }
    
    @Test
    public void compruebaProyectos() throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(Ong.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Ong o ;
        o = (Ong) unmarshaller.unmarshal(new File("ong.xml"));
        
        for ( Proyecto p : o.getProyectos()){
            assertNotNull("Proyecto nulo!", p);
        }
    }
    
    
}
