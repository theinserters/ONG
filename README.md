# The Inserters
## ICB0_P5 Programación orientada a objetos con acceso a base de datos

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/theinserters/ONG.git)


## Vista de la aplicación
<p align="center">
<img src="/documents/Aplicacion.jpeg" width="700px" >
</p>


### Productos
A continuación se enumeran todos los productos a llevar a cabo dentro del proyecto:
- Producto 1.
    - Diseño UML
    - Objetivo: Realizar Diagramas de casos de uso y profundizar en la
realización de Diagramas de clases para mostrar gráficamente el
comportamiento y el diseño estático del sistema
- Producto 2.
    - Implementación de clases en Java.
    - Objetivo: Implementar estructuras dinámicas de datos y el modelo
estático de clases en lenguaje Java, aplicar el patrón de diseño DAO,
almacenar datos en estructuras XML y realizar pruebas unitarias
utilizando JUnit, todo ello utilizando un sistema de control de versiones.
- Producto 3.
    - Persistencia en base de datos
    - Objetivo: Aplicar la persistencia en bases de datos usando los patrones
de diseño DAO y Factory.
- Producto 4.
    - Implementación de la aplicación de escritorio con interfaz gráfica.
    - Objetivo: Implementar una aplicación de escritorio con interfaz gráfica
utilizando el patrón de diseño MVC, JavaFX y la persistencia mediante
mapeo ORM JPA.


### Pequeña guia

- Producto1 : 
    - Retocar diagrama de casos de uso. Crear diagrama individual y consensual.

- Producto2:
    - Persistencia en ficheros XML, utilizar los patrones DAO y factory (para varios tipos de persistencias).
    - Nos centraremos en 3 clases:
	    - ONG: nif, nombre, direccion, fecha de creacion, arraylist de socios[agregacion]
	    - Socios
	    - ?

- Producto3:
    - Seguir utilizando los patrones DAO y factory.
    - Persistencia en una BBDD relacional, (base de datos simple y suficiente)
    - Utilizar JDBC (ejectuar instrucciones sql desde java y al revés)
    - Transactions, commit, roll back, procedimientos almacenados.

- Producto4:
    - Persistencia en BBDD utilizando JPA Hibernate (ORM)
    - Interfaz grafica con JavaFX
    - Aplicar el patrón Modelo-Vista-Controlador


> **Lo importante es aprender tecnicas de programacion!**


| Miembros |
| ------ |
| Oscar |
| Adri | 
| Javi | 


## Diagrama DAO y Factory
<p align="center">
<img src="/documents/Diagrama_DAO_y_Factory.png" width="700px" >
</p>


## License

MIT

**Free Software, Hell Yeah!**


   [inserters]: <https://github.com/JavierMoralesEstevez/The_Inserters>
